# triviaR

Soluciones a las trivias planteadas en el grupo de telegram:

# Colabora con la triviaR

Cualquiera puede proponer una trivia para el grupo, solo debes considerar que caiga en alguno de los niveles de esta escala:

![](img/escala_triviar.png)

- **[Basico -]**. Signo negativo, Ha instalado R, RStudio y unos paquetes, y ha hechos unas cuantas líneas de código.

- **[Básico]**. Sin signo. Conoce como crear vectores, crear objetos, importar datos, hacer operaciones básicas, calcular estadísticas descriptivas, explorar la información, extrae elementos de un objeto, flujo de trabajo en scripts.

- **[Básico +]**. Signo positivo, sabe hacer bucles, hace funciones básicas, usa la familia apply*(), flujo de trabajo en proyectos.

- **[Intermedio -]**. Signo negativo. Sabe hacer funciones vectorizadas, escribe código basado en reglas de estilo, flujo de trabajo en proyectos, conoce de control de versiones (no necesariamente lo usa), Conoce una lista de paquetes necesarios en su trabajo (no implica que los use eficientemente).

- **[Intermedio]**. Sin signo. Conoce los paquetes necesarios para su trabajo y los usa eficientemente, usa control de versiones y repositorios. Flujo de trabajos en proyectos. Puede participar en grupos de desarrollo con R.

- **[Intermedio +]**. Optimiza el código en R. Participa en equipos de desarrollo y puede hacerse cargo de tareas específicas. Identifica fácilmente errores o posibilidades de mejoras en códigos. Ha intentado hacer su primer paquete de R (Aunque no lo haya subido a CRAN)

- **[Avanzado inicial]**. Ha hecho paquetes y enviado a revisión en CRAN, Bioconductor u otros repositorios. Optimiza código. Dirige proyectos de desarrollo con R y otros lenguajes. Brinda servicios basados en tecnologías R o complementarias.

- **[Avanzado experto]**. Dirige grupos de desarrollo con R o cualquier otro lenguaje de base (c, c++, java, fortran, perl, python, etc). Revisor de paquetes para repositorios. Experto en control de versiones y sincronización con servicios de repositorios. Experto en CI/CD.

- **[Dios]**. Si eres uno de ellos: Douglas Bates, John Chambers, Peter Dalgaard, Robert Gentleman, Kurt Hornik, Ross Ihaka, Tomas Kalibera, Michael Lawrence, Friedrich Leisch, Uwe Ligges, Thomas Lumley, Martin Maechler, Martin Morgan, Paul Murrell, Martyn Plummer, Brian Ripley, Deepayan Sarkar, Duncan Temple Lang, Luke Tierney y Simon Urbanek.