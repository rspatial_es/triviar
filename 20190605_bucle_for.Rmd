---
title: "¿De qué otra forma se puede encontrar el resultado del siguiente código?"
author: '@gavg712'
date: "2019-06-05"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## datos del ejemplo

```{r cars}
set.seed(90)
x = runif(10000, min = 10, max = 15)
y = runif(100, min = 9, max = 12)
dim(x) <- c(100, 100)
```

## Código

```{r }
mdl_coefs <- data.frame()
for (i in seq_len(ncol(x))) {
  fit <- lm(y ~ x[,i])
  mdl_coefs[i, c("alpha", "beta", "r2")] <- c(coef(fit), summary(fit)$r.squared)
}
```

## Resultado

```{r }
head(mdl_coefs)
```

# Opción con `data.table` (@eliocamp)

```{r}
suppressPackageStartupMessages(library(data.table))
suppressPackageStartupMessages(library(reshape2))
suppressPackageStartupMessages(library(magrittr))

get_coef <- function(fit) {
  as.list(setNames(c(coef(fit), summary(fit)$r.squared), c("alpha", "beta", "r2")))
}

mdl_dt <- x %>% 
  melt() %>% 
  as.data.table() %>% 
  .[, get_coef(lm(y ~ value)), by  = Var2]

dplyr::all_equal(mdl_coefs, mdl_dt[,2:4])
```

# Opción con `tidyverse` y `broom` (@SGMStalin)

```{r}
suppressPackageStartupMessages(library(tidyverse))
suppressPackageStartupMessages(library(broom))
dt <- as_tibble(x, .name_repair = "minimal") %>% 
  bind_cols(., "y" = y) %>%
  gather("key", "value", V1:V100) %>%
  mutate(key = factor(key, levels = unique(key),
                      ordered = T))

mdl_coefs2 <- dt %>%
  group_by(key) %>%
  do(broom::tidy(lm(y ~ value, data = .), quick = T)) %>%
  spread(key = term, value = estimate) %>%
  inner_join(
    dt %>%
      group_by(key) %>%
      do(broom::glance(lm(y ~ value, data = .))) %>%
      select(r.squared), by = "key"
  ) %>%
  rename(alpha = `(Intercept)`, beta = value, r2 = r.squared)

all_equal(mdl_coefs, mdl_coefs2[,2:4])
```

# Opción con `tidyverse` (@gavg712)

```{r}
suppressPackageStartupMessages(library(tidyverse))

#creamos una función que haga la operación interna
get_lm_stats <- function(x, y) {
  fit <- lm(y ~ x)
  setNames(c(coef(fit), summary(fit)$r.squared), 
            c("alpha", "beta", "r2"))
}

# Reproducir los resultados del código de ejemplo con tidyverse
mdl_tidy <- bind_cols(tibble(y), 
          as_tibble(x, .name_repair = ~sprintf("V%03d", 1:100))) %>%
  gather(col, x, matches("^V")) %>%
  group_by(col) %>%
  group_map(~get_lm_stats(.x$x, .x$y) %>% bind_rows())

all_equal(mdl_coefs, mdl_tidy[,2:4])
```

# Opción con `base` (@eliocamp)

```{r}
mdl_base <- as.data.frame(t(
  apply(x, 2, function(x) {
    fit <- lm(y ~ x)
    setNames(c(coef(fit), summary(fit)$r.squared), 
             c("alpha", "beta", "r2"))
  })
))

dplyr::all_equal(mdl_coefs, mdl_base)
```

# Opción con `base` (@gavg712)

```{r}
mdl_base2 <- `colnames<-`(
  t(
    sapply(
      as.data.frame(x), 
      function(x) {
        fit <- lm(y ~ x)
        c(coef(fit), summary(fit)$r.squared)
      })),
    c("alpha", "beta", "r2"))
  
                    
dplyr::all_equal(mdl_coefs, mdl_base2)
```

# Opción con `base::Reduce` (@gavg712)

```{r}
mdl_base3 <- Reduce(rbind,
                    lapply(seq_len(ncol(x)),
                    function(i, X = x) {
                      fit <- lm(y ~ X[,i])
                      setNames(c(coef(fit), summary(fit)$r.squared),
                               c("alpha", "beta", "r2"))
                    }))
  
                    
dplyr::all_equal(mdl_coefs, mdl_base3)
```